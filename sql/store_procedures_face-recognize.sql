use db_face_recognize;



-- INSERTAR


delimiter $

create procedure sp_insertar_usuario(in nombre_completo varchar(40), in nombre_usuario
                                                        varchar(10), in clave varchar(10))

  begin

    insert into usuario (nombre_completo, nombre_usuario, clave)
    values (nombre_completo, nombre_usuario, clave);

  end $



delimiter $

create procedure sp_insertar_cliente(in identificacion varchar(15), in nombre_completo
                                                       varchar(40), in telefono varchar(15), in tipo int)

  begin

    insert into usuario (identificacion, nombre_completo, telefono, tipo)
    values (identificacion, nombre_completo, telefono, tipo);

  end $

DELIMITER $
CREATE PROCEDURE sp_agregar_deteccion(
  identificacion VARCHAR(15),
  hora_actual DATETIME
)
  BEGIN

    INSERT INTO detecciones(identificacion, hora_actual)
    VALUES (identificacion,hora_actual);

  END $


-- ELIMINAR



delimiter $

create procedure sp_eliminar_usuario(in id_usuario int)

  begin

    delete from usuario
    where usuario.id_usuario=id_usuario;

  end $



delimiter $

create procedure sp_eliminar_cliente(in identificacion varchar(15))

  begin

    delete from cliente
    where cliente.identificacion=identificacion;

  end $


-- ACTUALIZAR


delimiter $

create procedure sp_actualizar_usuario(in id_usuario int, in nombre_completo
                                                     varchar(40), in nombre_usuario varchar(10), in clave varchar(10))

  begin

    update usuario u set
      u.nombre_completo=nombre_completo,
      u.nombre_usuario=nombre_usuario,
      u.clave=clave
    where u.id_usuario=id_usuario;

  end $





delimiter $

create procedure sp_actualizar_cliente(in identificacion varchar(15), in nombre_completo
                                                         varchar(40), in telefono varchar(15), in tipo int)

  begin

    update cliente c set
      c.nombre_completo=nombre_completo,
      c.telefono=telefono,
      c.tipo=tipo
    where c.identificacion=identificacion;

  end $


-- CONSULTAS


delimiter $

create procedure sp_obtener_usuarios()

  begin

    select * FROM usuario;

  end $


delimiter $

create procedure sp_obtener_clientes()

  begin

    select * FROM cliente;

  end $
