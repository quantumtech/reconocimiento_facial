drop database if exists db_face_recognize;

create database db_face_recognize;

use db_face_recognize;


create table usuario(
	id_usuario int auto_increment primary key,
	nombre_completo varchar(40) not null,
	nombre_usuario varchar(10) not null,
	clave varchar(10)
);

create table sujeto(
	identificacion varchar(15) primary key,
  nombre_completo varchar(40) not null,
  telefono varchar(15) not null,
  tipo int default 0
);

CREATE TABLE detecciones(
  deteccion int AUTO_INCREMENT PRIMARY KEY ,
  identificacion VARCHAR(15),
  hora_actual DATETIME not NULL,
  FOREIGN KEY (identificacion) REFERENCES sujeto(identificacion)
);



