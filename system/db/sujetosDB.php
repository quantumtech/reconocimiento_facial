<?php
/**
 * Created by PhpStorm.
 * User: quark
 * Date: 23/11/17
 * Time: 10:07 AM
 */
include_once('db_conexion.php');

class sujetosDB
{
    var $conexion;

    function __construct($db_conexion)
    {
        $this->conexion= new db_conexion();
    }


    /**
     * @param $cedula cedula del sujeto
     * @param $nombre nombre completo
     * @param $telefono telefono del sujeto.
     * @param $fecha_nacimiento fecha de nacimiento en formato YYYY-MM-dd
     */
    function insertar_sujeto($cedula, $nombre, $telefono, $fecha_nacimiento){
        $sql = "call sp_insertar_sujeto('".$cedula."','".$nombre."','"
            .$telefono."','".$fecha_nacimiento."');";
        return $this->conexion->execute($sql);
    }

    function eliminar_sujeto($cedula)
    {
        $sql = "call sp_eliminar_sujeto('".$cedula."');";
        return $this->conexion->execute($sql);
    }

    function get_sujeto($cedula)
    {
        $sql = "call sp_get_sujeto('".$cedula."');";
        return $this->conexion->execute($sql);
    }


}