<?php
/**
 * Created by PhpStorm.
 * User: quark
 * Date: 23/11/17
 * Time: 10:18 PM
 */

?>
<html>
<head>
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
    <script src="../assets/js/bootstrap.bundle.min.js"></script>
    <title>Registrar Sujeto</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-light bg-faded">
        <a class="navbar-brand" href="#">
            <img src="../assets/img/Quantum.png" width="60" height="60" class="d-inline-block align-top">
        </a>
        <h4>QUARKTECH</h4>
    </nav>
    <?php
    if (isset($_GET['action'])) {
        switch ($_GET['action']) {
            case "registro": {
                if ($_GET['estado'] == 1 and $_GET['resultado_fotos'] = 1) {
                    $tipo = "alert-success";
                } else {
                    $tipo = "alert-danger";
                }
                ?>
                <div class="alert <?= $tipo ?> alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong><?= $_GET['mensaje'] ?></strong>
                </div>
                <?php
            }
                break;
        }
    }
    ?>

    <div class="row">
        <div class="card offset-sm-3 col-sm-6">
            <div class="card-header">
                <h5 class="card-title">Informacion Basica</h5>
            </div>
            <div class="card-body">
                <form method="post" action="../ln/op_registro_sujeto.php?action=registrar_sujeto"
                      enctype="multipart/form-data">
                    <div class="input-group-group">
                        <label for="cedula">Numero de cedula</label>
                        <input type="text" class="form-control" name="cedula" id="cedula"
                               placeholder="Cedula o pasaporte" required>
                    </div>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre"
                               placeholder="Nombre completo" required>
                    </div>
                    <div class="form-group">
                        <label for="telefono">Numero de telefono</label>
                        <input type="tel" class="form-control" name="telefono" id="telefono"
                               placeholder="Numero de telefono" required>
                    </div>
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento"
                               placeholder="Fecha de nacimiento"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="file" accept="application/zip" name="imagenes_zip" id="imagenes"
                               class="form-control btn-outline-dark" required>
                    </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-outline-dark">Registrar Sujeto</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

</body>
</html>
