<?php
/**
 * Created by PhpStorm.
 * User: quark
 * Date: 23/11/17
 * Time: 09:09 PM
 */
?>
<html>
<head>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <title>Sistema de reconocimiento Facial</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>
<body>
<div class="container">
    <nav class="navbar navbar-light bg-faded">
        <a class="navbar-brand" href="#">
            <img src="../assets/img/Quantum.png" width="100" height="100" class="d-inline-block align-top">
        </a>
        <h3>QUARKTECH</h3>
    </nav>
    <div class="container">
        <div class="card offset-md-3 col-sm-6">
            <h3 class="card-header text-center">Menu de acciones</h3>
            <ul class="list-group list-group-flush">
                <a href="../ui/registro_sujeto.php" class="list-group-item btn btn-outline-info"><h5>Registar Sujeto</h5></a>
                <a href="#" class="list-group-item btn btn-outline-info"><h5>Editar Sujeto</h5></a>
                <a href="#" class="list-group-item btn btn-outline-info"><h5>Alertas</h5></a>
                <a href="#" class="list-group-item btn btn-outline-info"><h5>Perfil</h5></a>

            </ul>

        </div>
    </div>

</body>
</html>
