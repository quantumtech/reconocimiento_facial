<?php
/**
 * Created by PhpStorm.
 * User: quark
 * Date: 24/11/17
 * Time: 12:43 AM
 */
require_once('upload.php');
require_once('../db/sujetosDB.php');

$upload = new upload();
$sujetos = new sujetosDB();

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'registrar_sujeto': {
            //Determinar si se logró subir el archivo
            $cedula = $_POST['cedula'];
            $nombre = $_POST['nombre'];
            $telefono = $_POST['telefono'];
            $fecha_nacimiento = $_POST['fecha_nacimiento'];
            $resultado_script = array();
            $archivo_guardado = $upload->cargar_archivo($cedula);
            $mensaje = "Sujeto registrado exitosamente";
            $insertado = 1;
            $resultado_fotos= 1;
            if ($archivo_guardado) {
                $ruta_principal = dirname(posix_getcwd(), 2);
                $ruta_script = $ruta_principal."/face_recognition_service/src/execution/";
                $script = "python3 ".$ruta_script."execute_procesar_zip.py -ced $cedula";
                exec($script, $resultado_script);
                //Ejecutar script
                $insertado = $sujetos->insertar_sujeto($cedula, $nombre, $telefono, $fecha_nacimiento);
            }

            if (!$insertado['exito']) {
                $insertado = 0;
                $mensaje = $insertado[0];
            }

            if ($resultado_script[6] = "False"){
                $resultado_fotos = 0;
                $mensaje = $mensaje."Error al cargar las imagenes: ".$resultado_script;
            }
            header("Location:../ui/registro_sujeto.php?action=registro&estado=
            $insertado&mensaje=$mensaje");
        }
            break;
    }
}
?>