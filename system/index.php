<?php
/**
 * Created by PhpStorm.
 * User: quark
 * Date: 23/11/17
 * Time: 03:46 PM
 */
?>
<html>
<head>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <title>Inicio de sesion</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0">
</head>
<body>

<div class="container">
    <nav class="navbar navbar-light bg-faded">
        <a class="navbar-brand" href="#">
            <img src="assets/img/Quantum.png" width="100" height="100" class="d-inline-block align-top">
        </a>
        <h3>QUARKTECH</h3>
    </nav>
    <div class="card offset-md-3 col-sm-6">
        <div class="card-header">
            <h3 class="card-title">Inicio de sesion</h3>
        </div>
        <div class="card-body">
                <form>
                    <div class="form-group">
                        <label for="username">Nombre de Usuario</label>
                        <input type="text" class="form-control" id="username" placeholder="Ingrese nombre de usuario">
                    </div>
                    <div class="form-group">
                        <label for="password">Clave de acceso</label>
                        <input type="password" class="form-control" id="password" placeholder="Digite su clave">
                    </div>
                    <button type="submit" class="btn btn-dark">Iniciar Sesion</button>
                </form>
        </div>
    </div>

</div>
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
