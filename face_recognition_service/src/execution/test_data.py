from src.lib.openface.data import Image
from src.ln.training import Training

image = Image("person", "image", "../../sources/dataset")

range = image.iterImgs("../../sources/dataset")

train = Training()

train.getImagesAndLabels("../../sources/dataset")

for indice in range:
    indi = str(indice)
    indi = indi.strip("()")
    indi = indi.replace(",", "/")
    indi = indi.replace(" ", "")
    print(indi)


