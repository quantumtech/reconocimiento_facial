import os, sys, inspect
currendir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currendir)
topdir = os.path.dirname(parentdir)
sys.path.insert(0, topdir)
import argparse
from src.ln.procesar_zip import ProcesarZIP

procesar = ProcesarZIP()
parser = argparse.ArgumentParser()
parser.add_argument("-ced", help ="cedula del sujeto")
argumento = parser.parse_args()
print(procesar.procesar_imagenes(str(argumento.ced)))
