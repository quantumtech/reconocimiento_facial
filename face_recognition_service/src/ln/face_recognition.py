# Import OpenCV2 for image processing
import os, sys, inspect

currendir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currendir)
topdir = os.path.dirname(parentdir)
sys.path.insert(0, topdir)
import cv2
from datetime import datetime, date, timedelta
import calendar
from src.lib.openface.align_dlib import AlignDlib
from src.db.conexion import Connection
from src.db.sujetos import Sujetos
from src.db.detecciones import Detecciones
from src.ln.training import Training
from src.ln.bot import EnvioMensajes


class FaceRecognizer:
    def __init__(self):
        # Create Local Binary Patterns Histograms for face recognization
        self.recognizer = cv2.face.LBPHFaceRecognizer_create()
        # Load the trained model
        self.training = Training().train(topdir + "/sources/dataset/")
        self.recognizer.read(topdir + "/sources/trainer/trainer.yml")
        self.align = AlignDlib(topdir + "/sources/models/shape_predictor_68_face_landmarks.dat")
        self.alertas = []
        self.connection = Connection("localhost", "root", "", "face_recognition")
        self.sujetos = Sujetos(self.connection)
        self.detecciones = Detecciones(self.connection)
        self.bot = EnvioMensajes()

    def get_person_id(self, rgb_image):
        """
        Determina si el rostro recibido es conocido por el sistema
        mediante el analisis del histograma de la imagen
        :param rgb_image: Imagen en formato rgb .jpg.png
        :type rgb_image: str
        :return: cedula de la persona identificada
        """
        cedula = None
        gray = cv2.cvtColor(rgb_image, cv2.COLOR_BGR2GRAY)
        rostro = self.align.align(150, gray)
        # Convert the captured frame into grayscale

        if rostro is not None:
            identificador = self.recognizer.predict(rostro)

            # Recognize the face belongs to which ID
            print(identificador)
            if int(identificador[0]) > 1 and int(identificador[1]) < 70:
                cedula = identificador[0]

        return cedula

    def verificar_alerta(self, cedula):
        """
        :param cedula: cedula del sujeto
        :type cedula: bool
        :return: True si la alerta se envio hace menos de 30 minutos, False si no
        """
        result = False
        timestamp = datetime.now()
        for alerta in self.alertas:
            if alerta["cedula"] == cedula:
                hora_alerta = alerta["timestamp"] + timedelta(minutes=30)
                if hora_alerta > timestamp:
                    result = True
        return result

    def enviar_alerta(self, cedula, ruta_imagen):

        if self.verificar_alerta(cedula) is False:
            hora_actual = datetime.now()
            self.detecciones.agregar_deteccion(cedula, hora_actual)
            self.alertas.append({"cedula": cedula, 'timestamp': hora_actual})

            self.bot.enviar_imagen(self.generar_mensaje(cedula, hora_actual), ruta_imagen)
            print(self.alertas)

    def generar_mensaje(self, cedula, hora_actual):

        sujeto = self.sujetos.get_sujeto(cedula)[1][0]
        print(sujeto)
        mensaje = " " + sujeto["nombre"] + " cedula: " + str(sujeto["cedula"]) + " nacimiento: " \
                  + str(sujeto["fecha_nacimiento"]) + ". detectado a las: " + str(hora_actual)

        return mensaje