# Import OpenCV2 for image processing
# Import os for file path
import cv2
#Configuracion del path
import os, sys, inspect
currendir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currendir)
topdir = os.path.dirname(parentdir)
sys.path.insert(0, topdir)

# Import numpy for matrix calculation
import numpy as np
from src.lib.openface.data import Image as Img
import dlib


class Training:
    def __init__(self):

        # Create Local Binary Patterns Histograms for face recognization
        self.recognizer = cv2.face.LBPHFaceRecognizer_create()

        # Using prebuilt frontal face training model, for face detection
        self.detector = cv2.CascadeClassifier("../../sources/haarcascades/haarcascade_frontalface_default.xml")
        self.img = Img("", "", "")
        self.dlibdetector = dlib.get_frontal_face_detector()

    def rgb_pre_processing(self, image):
        """Performs CLAHE on each RGB components and rebuilds final
        normalised RGB image - side bnote: improved face detection not recognition"""
        (h, w) = image.shape[:2]
        zeros = np.zeros((h, w), dtype="uint8")
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(10, 10))
        (B, G, R) = cv2.split(image)
        R = clahe.apply(R)
        G = clahe.apply(G)
        B = clahe.apply(B)
        filtered = cv2.merge([B, G, R])
        return filtered

    def pre_processing(self, image):
        """Performs CLAHE on a greyscale image"""
        clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        cl1 = clahe.apply(image)
        return cl1

    def detect_dlib_face(self, image):
        image = self.pre_processing(image)
        bbs = self.dlibdetector(image, 1)
        return bbs

        # Create method to get the images and label data
    def get_images_labels(self, path):
        """
        Funcion que permite analizar el dataset y asi entrenar el sistema

        :param path: Ruta de la carpeta dataset
        :type path:str
        :return: faceSamples: Lista con los rostros analizados
        :return: ids: id de los rostros
        :rtype:
        """
        # Get all file path
        imagePaths = []
        ruta = self.img.iterImgs(path)

        for indice in ruta:
            indice = str(indice)
            imagePaths.append([indice[1:indice.index(",")], indice[indice.index(",") + 2:len(indice)-1]])

        # Initialize empty face sample
        faceSamples = []

        # Initialize empty id
        ids = []

        # Loop all the file path
        for imagePath in imagePaths:
            # Get the image in grayscale
            try:
                img = cv2.imread(path+imagePath[0]+"/"+imagePath[1], 0)
                ids.append(int(imagePath[0]))
                faceSamples.append(img)
            except ValueError:
                break

        # Pass the face array and IDs array

        return faceSamples, ids

        # Get the faces and IDs

    def train(self, path):
        faces, ids = self.get_images_labels(path)
        # Train the model using the faces and IDs
        self.recognizer.train(faces, np.array(ids))
        # Save the model into trainer.yml
        self.recognizer.save(topdir+'/sources/trainer/trainer.yml')
