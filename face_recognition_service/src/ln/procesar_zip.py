import cv2
import os, sys, inspect

currendir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currendir)
topdir = os.path.dirname(parentdir)
sys.path.insert(0, topdir)
from src.lib.openface import helper, AlignDlib
from src.lib.openface.data import Image as Img

import zipfile


class ProcesarZIP:
    def __init__(self):
        self.img = Img("", "", "")
        self.align = AlignDlib(topdir+"/sources/models/shape_predictor_68_face_landmarks.dat")

    def descomprimir(self, cedula):
        path = topdir+"/temp_photos/" + cedula + "/"
        archivo = path + cedula + ".zip"
        print(archivo)
        password = None
        archivo = zipfile.ZipFile(archivo, "r")

        try:
            archivo.extractall(path, password)
        except Exception as e:
            print(e)

        archivo.close()

    def obtener_nombres(self, cedula):
        """

        :param cedula: cedula del sujeto
        :type cedula: string
        :return: array con el nombre de las fotos del sujeto
        """

        path = topdir+"/temp_photos/" + cedula + "/"
        ruta = self.img.iterImgs(path)
        image_paths = []
        for indice in ruta:
            indice = str(indice)
            image_paths.append(indice[indice.index(",") + 2:len(indice) - 1])
        return image_paths

        # create folder to store photos

    def create_folder(self, cedula):
        path = topdir+"/sources/dataset/" + cedula
        try:
            helper.mkdirP(path)
        except Exception as e:
            print(e)

    def procesar_imagenes(self, cedula):
        estado = True
        self.descomprimir(cedula)
        print("zip descomprimido")
        imagenes = self.obtener_nombres(cedula)
        print("image_paths")
        self.create_folder(cedula)  # folder para almacenar imagenes
        print("folder creado")
        path = topdir+"/temp_photos/" + cedula + "/"
        path2 = topdir+"/sources/dataset/" + cedula
        count = 1
        try:
            print("Iniciando procesamiento de imagenes")
            for imagen in imagenes:
                gray = cv2.imread(path + imagen, 0)
                rostro = self.align.align(150, gray)
                if rostro is not None:
                    cv2.imwrite(path2 + "/image-" + str(count) + ".jpg",
                            self.align.align(150, rostro))
                    count += 1
            print("Imagenes procesadas")

        except Exception as e:
            estado = False
        return estado
