# Import OpenCV2 for image processing
import cv2
import os, sys, inspect
currendir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currendir)
topdir = os.path.dirname(parentdir)
sys.path.insert(0, topdir)

from src.lib.openface import helper, AlignDlib

class FaceDatasets:

    def __init__(self):
        face_detector = cv2.CascadeClassifier('../../sources/haarcascade_frontalface_default.xml')
        self.align = AlignDlib("../../sources/models/shape_predictor_68_face_landmarks.dat")

    # create folder to store photos
    def create_folder(self, path):
        helper.mkdirP(path)

    def take_photos(self, face_id, camara, cantidad_fotos):
        """
            Funcion que permite tomar la cantidad de fotos deseada,
            eligiendo 0 se utiliza la webcam integrada y uno para una externa.
            Face id representa el numero de cedula del cliente

            :param face_id: Cedula del cliente
            :type face_id : int
            :param camara: especifica si se utilizara la camara integrada
            :type camara: int
            :param cantidad_fotos: Cantidad de fotos a tomar
            :type cantidad_fotos: int
            """
        # Start capturing video
        path = "../../sources/dataset/" + str(face_id)

        self.create_folder(path)

        vid_cam = cv2.VideoCapture(camara)

        # Detect object in video stream using Haarcascade Frontal Face
        face_detector = cv2.CascadeClassifier('../../sources/haarcascade_frontalface_default.xml')

        # Initialize sample face image
        count = 0

        # Start looping
        while True:

            # Capture video frame
            _, image_frame = vid_cam.read()

            # Convert frame to grayscale
            gray = cv2.cvtColor(image_frame, cv2.COLOR_BGR2GRAY)


            # Increment sample face image
            count += 1

            # Save the captured image into the dataset folder
            print(cv2.imwrite(path + "/image-" + str(count) + ".jpg",
                              self.align.align(150, gray)))
            # Display the video frame, with bounded rectangle on the person's face
            cv2.imshow('frame', image_frame)

            # To stop taking video, press 'q' for at least 100ms
            if cv2.waitKey(50) & 0xFF == ord('q'):
                break

            # If image taken reach 100, stop taking video
            elif count > cantidad_fotos:
                break

        # Stop video
        vid_cam.release()

        # Close all started windows
        cv2.destroyAllWindows()


