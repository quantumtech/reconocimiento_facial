# Import OpenCV2 for image processing
import cv2, dlib
from src.lib.openface.align_dlib import AlignDlib

class FaceRecognizer:

    def __init__(self):
        # Create Local Binary Patterns Histograms for face recognization
        self.recognizer = cv2.face.LBPHFaceRecognizer_create()
        # Load the trained mode
        self.recognizer.read("../../sources/trainer/trainer.yml")
        # Set the font style
        self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.align = AlignDlib("../../sources/models/shape_predictor_68_face_landmarks.dat")
        # Initialize and start the video frame capture
        self.cam = cv2.VideoCapture(0)

    def get_person_id(self):
        # Loop
        while True:
            # Read the video frame
            #ret, im = cam.read()
            gray = cv2.cvtColor(self.im, cv2.COLOR_BGR2GRAY)
            rostro = self.align.align(150, gray)
            # Convert the captured frame into grayscale
            landmarks = self.align.getAllFaceBoundingBoxes(self.im)
            if len(landmarks) > 0 and rostro is not None:
                for k, d in enumerate(landmarks):
                    cv2.rectangle(self.im, (d.left(), d.top()),
                                  (d.right(), d.bottom()), (100, 255, 0), 4)

                Id = self.recognizer.predict(rostro)
                # Recognize the face belongs to which ID
                print(Id)
                # Check the ID if exist
                if(Id[0] == 702420599):
                        Id = "Diego"
                elif (Id[0] == 702520324):
                    Id = "Tatiana"
                elif (Id[0] == 703010596):
                    Id = "Steven"
                elif (Id[0] == 70000):
                    Id = "Nicole"
                elif (Id[0] == 15514909335):
                    Id = "Mirna"
                #If not exist, then it is Unknown
                else:
                    print(Id)
                    Id = "Unknow"

                # Put text describe who is in the picture
                cv2.putText(self.im, str(Id), (d.left(), d.top()), self.font, 2, (255,255,255), 3)

                # Display the video frame with the bounded rectangle
                cv2.imshow('im',self.im)
                # If 'q' is pressed, close program
            else:
                print("no se detecto rostro")
                cv2.imshow('im', self.im)
            if cv2.waitKey(100) & 0xFF == ord('q'):
                break

        # Stop the camera
        self.cam.release()

        # Close all windows
        cv2.destroyAllWindows()
