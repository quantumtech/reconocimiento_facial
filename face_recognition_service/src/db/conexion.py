import datetime
import pymysql

class Connection:

    db = None
    cursor = None
    host = None
    user = None
    password = None
    database = None
    charset = None
    connection = None

    def __init__(self, host, user, password, database, charset):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.charset = charset

    def conectar(self):
        try:
            self.connection = \
                pymysql.connect(host=self.host, user=self.user,password= self.password,
                                db = self.database,charset = self.charset,
                                cursorclass=pymysql.cursors.DictCursor)
            return True, "Conexion establecida"

        except Exception as e:
            return False, e

    def datetime_handler(self, x):
        if isinstance(x, datetime.datetime) or isinstance(x, datetime.date):
            return str(x)
        raise TypeError("Unknown type")

    def get_data(self, sql):
        try:
            self.conectar()
            self.cursor.execute(sql)
            desc = self.cursor.description
            data = [dict(zip([col[0] for col in desc], row))
                    for row in self.cursor.fetchall()]
            self.connection.close()
            return True, data
        except Exception as e:
            return False, e


    def set_data(self, sql):
        try:
            self.conectar()
            with self.connection.cursor() as cursor:
                cursor.execute(sql, None)
            self.connection.commit()
            self.connection.close()
            return True, "Exito"

        except Exception as e:
            print (e)
            return False, e
