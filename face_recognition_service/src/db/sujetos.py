import json
from src.db.conexion import Connection

class Sujetos:
    connection = None

    def __init__(self, connection):
        """
        Metodo de iniciacion de la la instancia

        """
        self.connection = connection

    def get_sujeto(self, cedula):
        sql = "call sp_get_sujeto('"+str(cedula)+"');"
        return self.connection.get_data(sql)

    def delete_sujeto(self, cedula):
        sql = "call sp_delete_sujeto('"+str+"');"
