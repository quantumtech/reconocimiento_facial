from datetime import datetime
class Detecciones:
    connection = None

    def __init__(self, conecction):
        self.connection = conecction

    def agregar_deteccion(self, cedula, hora_actual):
        """
        :param hora_actual: momento en el que se da la deteccion del sujeto
        :type hora_actual: datetime
        :param cedula: Cedula del sujeto detectado
        :type cedula: int
        :return: estado de la consulta
        """
        sql = "call sp_agregar_deteccion('" + str(cedula) + "','" + str(hora_actual) + "');"
        return self.connection.set_data(sql)
